// select the can from HTMl
const can = document.getElementById("table");
const ctx = can.getContext('2d');


//Ball object
const ball = {
    x : can.width/2,
    y : can.height/2,
    radius : 10,
    velocityX : 5,
    velocityY : 5,
    speed : 7,
    color : "#fef24e"
}

//User Paddle
const user = {
    x : 0, // left side of canvas
    y : (can.height - 100)/2, // subtracting the height of paddle
    width : 10,
    height : 100,
    score : 0,
    color : "crimson"
}

//CPU Paddle
const cpu = {
    x : can.width - 10, // subtracting the width of paddle
    y : (can.height - 100)/2, // Subtracting the height of paddle
    width : 10,
    height : 100,
    score : 0,
    color : "crimson"
}

//Draw  seperator
const seperator = {
    x : (can.width - 2)/2,
    y : 0,
    height : 10,
    width : 2,
    color : "white"
}

//draw a rectangle, used to draw paddles
function drawRect(x, y, w, h, color){
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);
}

//draw circle, used to draw the ball
function drawArc(x, y, r, color){
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.arc(x,y,r,0,Math.PI*2,true);
    ctx.closePath();
    ctx.fill();
}

//Making the event listener- to control the user paddle with the help of mouse
can.addEventListener("mousemove", getMousePos);

function getMousePos(evt){
    let rect = can.getBoundingClientRect();
    
    user.y = evt.clientY - rect.top - user.height/2;
}

//when CUP or USER scores, we reset the ball
function restart(){
    ball.x = can.width/2;
    ball.y = can.height/2;
    ball.velocityX = -ball.velocityX;
    ball.speed = 7;
}

//draw the seperator
function drawseperator(){
    for(let i = 0; i <= can.height; i+=15){
        drawRect(seperator.x, seperator.y + i, seperator.width, seperator.height, seperator.color);
    }
}

//draw Scores
function drawScore(text,x,y){
    ctx.fillStyle = "white";
    ctx.font = "75px fantasy";
    ctx.fillText(text, x, y);
}

// collision detection
function collision(b,p){
    //At top
    p.top = p.y;
    //At bottom
    p.bottom = p.y + p.height;
    //At left
    p.left = p.x;
    //At right
    p.right = p.x + p.width;
    
    //At top
    b.top = b.y - b.radius;
    //At bottom
    b.bottom = b.y + b.radius;
    //At left
    b.left = b.x - b.radius;
    //At right
    b.right = b.x + b.radius;
    
    //If the following statement is true the collision happened
    return p.left < b.right && p.top < b.bottom && p.right > b.left && p.bottom > b.top;
}

//update function, that does all calculations
function update(){
    
    //change the score of players, if the ball goes to the left "ball.x<0" computer win, else if "ball.x > can.width" the user win
    if( ball.x - ball.radius < 0 ){
        cpu.score++;
        restart();
    }else if( ball.x + ball.radius > can.width){
        user.score++;
        restart();
    }
    
    //velocity of the ball
    ball.x += ball.velocityX;
    ball.y += ball.velocityY;
    
    //Making the computer paddle move itself
    cpu.y += ((ball.y - (cpu.y + cpu.height/2)))*0.1;
    
    //when the ball collides with bottom and top walls we inverse the y velocity.
    if(ball.y - ball.radius < 0 || ball.y + ball.radius > can.height){
        ball.velocityY = -ball.velocityY;
    }
    
    //we check if the paddle hit the user or the cpu paddle
    let player = (ball.x + ball.radius < can.width/2) ? user : cpu;
    
    //if the ball hits a paddle
    if(collision(ball,player)){
    
        //checking where the ball hits the paddle
        let collidePoint = (ball.y - (player.y + player.height/2));

        //normalize the value of collidePoint
        collidePoint = collidePoint / (player.height/2);
        
        //Changing the direction of the ball at 45Degrees using sine and cosine function.
        //Math.PI/4 = 45degrees
        let angleRad = (Math.PI/4) * collidePoint;
        
        //change the X and Y velocity direction
        let direction = (ball.x + ball.radius < can.width/2) ? 1 : -1;
        ball.velocityX = direction * ball.speed * Math.cos(angleRad);
        ball.velocityY = ball.speed * Math.sin(angleRad);
        
        //speed up the ball everytime a paddle hits it.
        ball.speed += 0.1;
    }
}

//Helper function, the function that does al the drawing
function helper(){
    
    //clear the can
    drawRect(0, 0, can.width, can.height, "#000");
    
    //draw the user score to the left
    drawScore(user.score,can.width/4,can.height/5);
    
    // draw the cpu score to the right
    drawScore(cpu.score,3*can.width/4,can.height/5);
    
    //draw the seperator
    drawseperator();
    
    //draw the user's paddle
    drawRect(user.x, user.y, user.width, user.height, user.color);
    
    //draw the COM's paddle
    drawRect(cpu.x, cpu.y, cpu.width, cpu.height, cpu.color);
    
    //draw the ball
    drawArc(ball.x, ball.y, ball.radius, ball.color);
}

//gameplay
function game(){
    update();
    helper();
}

//number of frames per second
let framePerSecond = 50;

//call the game function 50 times every 1 Sec
let loop = setInterval(game,1000/framePerSecond);